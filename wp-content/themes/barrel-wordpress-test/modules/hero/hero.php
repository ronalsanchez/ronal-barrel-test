<?php
$homepage = get_page_by_path('front-page', null, 'page');
$image = get_the_post_thumbnail_url($homepage->ID, 'large');
?>
<section class="hero" data-module="hero">
	<main>
		<container style="background-image:url('<?php echo $image; ?>');">
			<div class="flex-item wrapper">
				<?php 
				$page_id = 2;                                         
				$page = get_post($page_id);                        
				$content = $page->post_content;
				?>
			<div class="flex-container">
				<?php echo $content; ?>
			</div>
			</div>
			<div class="flex-item article">
				<?php
				$recent = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>1));
				while ( $recent->have_posts() ) {
				$recent->the_post();
				the_module('hero-post');
 			 	}
			 	wp_reset_postdata(); 
		 		?>
		 </div>
</container>
	</main>
	<div>			
	</div>	
</section>